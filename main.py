# Integrantes:
# Brayan Stiben Fonseca Hernandez (506222714)
# Lisseth Tatiana Quilindo Patiño (506222011)
# Daniel Esteban Torres Triana / 506222711
# Estructuras de datos

from fastapi import FastAPI, Query
import mook
from faker import Faker
import numpy as np
from pydantic import BaseModel
from typing import Optional
from fastapi.responses import JSONResponse
from music import songs_data

app = FastAPI()

class RangeRequest(BaseModel):
    start: int
    end: int

def merge_sort(arr):
    if len(arr) <= 1:
        return arr
    mid = len(arr) // 2
    left = merge_sort(arr[:mid])
    right = merge_sort(arr[mid:])
    return merge(left, right)

def merge(left, right):
    merged = []
    left_index = 0
    right_index = 0
    while left_index < len(left) and right_index < len(right):
        if left[left_index] < right[right_index]:
            merged.append(left[left_index])
            left_index += 1
        else:
            merged.append(right[right_index])
            right_index += 1
    merged.extend(left[left_index:])
    merged.extend(right[right_index:])
    return merged

def generate_sentences():

    fake = Faker()
    frases = []
    for i in range(500):
        frase = f"{fake.sentence()}"
        frases.append(frase)

    frases.sort()
    alfabeto = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ') * 20
    frases_numeradas = [f"{alfabeto[i]}. {frase}" for i, frase in enumerate(frases)]
    frases_array = np.array(frases_numeradas)

    return frases_array

@app.get("/")
def root():
    return {
        "Servicio": "Estructuras de datos"
    }

@app.post("/indices-invertidos")
def indices_invertidos(palabra: dict):
    cache = {}
    for index, documents in enumerate(mook.my_documents):
        sentences = documents.split()
        for sentence in sentences:
            words = sentence.lower().split()
            for word in words:
                if word in cache:
                    cache[word].append(documents)
                else:
                    cache[word] = [documents]
    return cache.get(palabra["palabra"], "No se encontró")

@app.post("/encontrar-numero-repetido")
def encontrar_numero_repetido(palabra: dict):
    lista_de_numeros = palabra["lista_de_numeros"]
    indices = {}
    tortuga = lista_de_numeros[0]
    conejo = lista_de_numeros[0]
    while True:
        tortuga = lista_de_numeros[tortuga]
        conejo = lista_de_numeros[lista_de_numeros[conejo]]
        if tortuga == conejo:
            break
    tortuga = lista_de_numeros[0]
    while tortuga != conejo:
        tortuga = lista_de_numeros[tortuga]
        conejo = lista_de_numeros[conejo]

    numero_repetido = tortuga

    for i, num in enumerate(lista_de_numeros):
        if num == numero_repetido:
            if num in indices:
                indices[num].append(i)
            else:
                indices[num] = [i]

    return {"NumeroRepetido": numero_repetido, "IndicesRepetidos": indices.get(numero_repetido, [])}

@app.post("/ordenar-frases")
def ordenar_frases(rango: RangeRequest):
    frases_array = generate_sentences()
    rango_ordenado = merge_sort(frases_array[rango.start:rango.end])
    return {"FrasesOrdenadas": rango_ordenado}

@app.get("/canciones/")
async def get_songs(emocion: Optional[str] = Query(None, description="Filtrar por emoción")):
    if emocion is not None and emocion in songs_data["Sentimientos"]:
        return JSONResponse(content={emocion: songs_data["Sentimientos"][emocion]})
    else:
        return songs_data
